# class are namespaces
class MyFunctions:
	def exp(self):
		return 0

ff = MyFunctions()
print ff.exp()

# magic functions - constructor
# class Atom:
# 	def __init__(self,*param): # multiple opertor
# 		self.atno = param[0]
# 		self.position = param[1:4]
# 		self.symbol = {2: 'He'} # dictionary

# at = Atom(2,2,3,4)
# print at.position

# magic functions - constructor
class Atom:
	def __init__(self,*param): # multiple opertor
		self.atno = param[0]
		self.position = param[1:4]
		self.symbol = {2: 'He'} # dictionary
	def getSymbol(self):
		return self.symbol[self.atno]
	def __repr__(self): # overloading printing
		# formatting values into strings
		return 'atno: %s, position: (%d, %d, %d)' % (self.atno,
		self.position[0], self.position[1], self.position[2]) 

# at = Atom(2,2,3,4)
# print at.getSymbol()
# print at # usage of overloaded function

# using classes inside classes (containers)
class Molecule:
	def __init__(self, name = 'Generic'): # default assignment
		self.name = name # argument
		self.atomList = [] # type is not yet defined
	def addAtom(self, atom):
		self.atomList.append( atom ) # append an object atom
	def __repr__(self): # object representations
		str = 'This is a molecule with name %s\n' % self.name
		str = str + 'It has %d atoms\n' % len(self.atomList)
		for atom in self.atomList:
			print atom
		return str

mol = Molecule('Water')
at = Atom(8,0,0,0)
mol.addAtom(at)
mol.addAtom(Atom(1,0,0,1))
print mol

# inheritance - another pillar of object oriented programming
class QM_Molecule(Molecule): # argument defines inheritance
	def __init__(self, name = "Generic", basis = Atom(1,0,0,1)):
		self.basis = []
		self.basis.append( basis )
		Molecule.__init__(self,name)
	def setBasis(self):
		for atom in self.atomList:
			self.basis.append( atom )

qmol = QM_Molecule('Hydrogen')
qmol.addAtom(Atom(1,0,0,1))
qmol.addAtom(Atom(2,0,0,2))
qmol.setBasis()